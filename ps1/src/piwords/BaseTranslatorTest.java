package piwords;

import static org.junit.Assert.*;

import org.junit.Test;

public class BaseTranslatorTest {
    @Test
    public void basicBaseTranslatorTest() {
        // Expect that .01 in base-2 is .25 in base-10
        // (0 * 1/2^1 + 1 * 1/2^2 = .25)
        int[] input = {0, 1};
        int[] expectedOutput = {2, 5};
        assertArrayEquals(expectedOutput,
                          BaseTranslator.convertBase(input, 2, 10, 2));
        
        int[] input2 = {7, 7};
        int[] expectedOutput2 = {12, 5};
        assertArrayEquals(expectedOutput2,
                          BaseTranslator.convertBase(input2, 10, 16, 2));

	    int[] input3 = {1, 1, 1, 1 , 1};
	    int[] expectedOutput3 = {15, 8, 0, 0};
	    assertArrayEquals(expectedOutput3,
	                      BaseTranslator.convertBase(input3, 2, 16, 4));
	    
	    int[] input4 = {11, 10, 13};
	    int[] expectedOutput4 = {7, 2, 9};
	    assertArrayEquals(expectedOutput4,
	                      BaseTranslator.convertBase(input4, 16, 10, 3));
}
}
