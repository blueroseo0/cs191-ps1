package piwords;

public class AlphabetGenerator {
    /**
     * Given a numeric base, return a char[] that maps every digit that is
     * representable in that base to a lower-case char.
     * 
     * This method will try to weight each character of the alphabet
     * proportional to their occurrence in words in a training set.
     * 
     * This method should do the following to generate an alphabet:
     *   1. Count the occurrence of each character a-z in trainingData.
     *   2. Compute the probability of each character a-z by taking
     *      (occurrence / total_num_characters).
     *   3. The output generated in step (2) is a PDF of the characters in the
     *      training set. Convert this PDF into a CDF for each character.
     *   4. Multiply the CDF value of each character by the base we are
     *      converting into.
     *   5. For each index 0 <= i < base,
     *      output[i] = (the first character whose CDF * base is > i)
     * 
     * A concrete example:
     * 	 0. Input = {"aaaaa..." (302 "a"s), "bbbbb..." (500 "b"s),
     *               "ccccc..." (198 "c"s)}, base = 93
     *   1. Count(a) = 302, Count(b) = 500, Count(c) = 193
     *   2. Pr(a) = 302 / 1000 = .302, Pr(b) = 500 / 1000 = .5,
     *      Pr(c) = 198 / 1000 = .198
     *   3. CDF(a) = .302, CDF(b) = .802, CDF(c) = 1
     *   4. CDF(a) * base = 28.086, CDF(b) * base = 74.586, CDF(c) * base = 93
     *   5. Output = {"a", "a", ... (28 As, indexes 0-27),
     *                "b", "b", ... (47 Bs, indexes 28-74),
     *                "c", "c", ... (18 Cs, indexes 75-92)}
     * 
     * The letters should occur in lexicographically ascending order in the
     * returned array.
     *   - {"a", "b", "c", "c", "d"} is a valid output.
     *   - {"b", "c", "c", "d", "a"} is not.
     *   
     * If base >= 0, the returned array should have length equal to the size of
     * the base.
     * 
     * If base < 0, return null.
     * 
     * If a String of trainingData has any characters outside the range a-z,
     * ignore those characters and continue.
     * 
     * @param base A numeric base to get an alphabet for.
     * @param trainingData The training data from which to generate frequency
     *                     counts. This array is not mutated.
     * @return A char[] that maps every digit of the base to a char that the
     *         digit should be translated into.
     */
    public static char[] generateFrequencyAlphabet(int base,
                                                   String[] trainingData) {
    	int sLength=trainingData.length;
    	int cLength;
    	int i,j;
    	int [] occurence= new int[26];
    	char [] ca=new char[1000];
    	for (i=0;i<sLength;i++){
    		ca=trainingData[i].toCharArray();
    		cLength=ca.length;
    		for (j=0;j<cLength;j++){
    			switch (ca[j]){
    			case 'a': occurence[0]++; break;
    			case 'b': occurence[1]++; break;
    			case 'c': occurence[2]++; break;
    			case 'd': occurence[3]++; break;
    			case 'e': occurence[4]++; break;
    			case 'f': occurence[5]++; break;
    			case 'g': occurence[6]++; break;
    			case 'h': occurence[7]++; break;
    			case 'i': occurence[8]++; break;
    			case 'j': occurence[9]++; break;
    			case 'k': occurence[10]++; break;
    			case 'l': occurence[11]++; break;
    			case 'm': occurence[12]++; break;
    			case 'n': occurence[13]++; break;
    			case 'o': occurence[14]++; break;
    			case 'p': occurence[15]++; break;
    			case 'q': occurence[16]++; break;
    			case 'r': occurence[17]++; break;
    			case 's': occurence[18]++; break;
    			case 't': occurence[19]++; break;
    			case 'u': occurence[20]++; break;
    			case 'v': occurence[21]++; break;
    			case 'w': occurence[22]++; break;
    			case 'x': occurence[23]++; break;
    			case 'y': occurence[24]++; break;
    			case 'z': occurence[25]++; break;
    			default: return null;
    			}
    		}
    		
    	}
    	int sum=0;
    	for (i=0; i<26; i++) sum+=occurence[i];
    	double[] pr= new double [26];
    	double[] cdf= new double [26];
    	for (i=0; i<26; i++) {
    		pr[i]=(double)occurence[i]/(double)sum;
    		for (j=0; j<=i; j++){
    			cdf[i]+=pr[j];
    		}
    		cdf[i]*=base;
    		if(cdf[i]%1>=.5){cdf[i]=Math.ceil(cdf[i]);}
    		else{cdf[i]=Math.floor(cdf[i]);}
    	}
    	int totalctr=0;
    	char[] output=new char[base];
    	while(totalctr<cdf[0]){output[totalctr]='a';totalctr++;}
    	while(totalctr<cdf[1]){output[totalctr]='b';totalctr++;}
    	while(totalctr<cdf[2]){output[totalctr]='c';totalctr++;}
    	while(totalctr<cdf[3]){output[totalctr]='d';totalctr++;}
    	while(totalctr<cdf[4]){output[totalctr]='e';totalctr++;}
    	while(totalctr<cdf[5]){output[totalctr]='f';totalctr++;}
    	while(totalctr<cdf[6]){output[totalctr]='g';totalctr++;}
    	while(totalctr<cdf[7]){output[totalctr]='h';totalctr++;}
    	while(totalctr<cdf[8]){output[totalctr]='i';totalctr++;}
    	while(totalctr<cdf[9]){output[totalctr]='j';totalctr++;}
    	while(totalctr<cdf[10]){output[totalctr]='k';totalctr++;}
    	while(totalctr<cdf[11]){output[totalctr]='l';totalctr++;}
    	while(totalctr<cdf[12]){output[totalctr]='m';totalctr++;}
    	while(totalctr<cdf[13]){output[totalctr]='n';totalctr++;}
    	while(totalctr<cdf[14]){output[totalctr]='o';totalctr++;}
    	while(totalctr<cdf[15]){output[totalctr]='p';totalctr++;}
    	while(totalctr<cdf[16]){output[totalctr]='q';totalctr++;}
    	while(totalctr<cdf[17]){output[totalctr]='r';totalctr++;}
    	while(totalctr<cdf[18]){output[totalctr]='s';totalctr++;}
    	while(totalctr<cdf[19]){output[totalctr]='t';totalctr++;}
    	while(totalctr<cdf[20]){output[totalctr]='u';totalctr++;}
    	while(totalctr<cdf[21]){output[totalctr]='v';totalctr++;}
    	while(totalctr<cdf[22]){output[totalctr]='w';totalctr++;}
    	while(totalctr<cdf[23]){output[totalctr]='x';totalctr++;}
    	while(totalctr<cdf[24]){output[totalctr]='y';totalctr++;}
    	while(totalctr<cdf[25]){output[totalctr]='z';totalctr++;}
    	
    	
    	return output;
    	
    }
}
