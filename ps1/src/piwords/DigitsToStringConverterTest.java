package piwords;

import static org.junit.Assert.*;

import org.junit.Test;

public class DigitsToStringConverterTest {
    @Test
    public void basicNumberSerializerTest() {
        // Input is a 4 digit number, 0.123 represented in base 4
        int[] input = {0, 1, 2, 3};
        char[] alphabet = {'d', 'c', 'b', 'a'};

        String expectedOutput = "dcba";
        assertEquals(expectedOutput,
                     DigitsToStringConverter.convertDigitsToString(
                             input, 4, alphabet));
        
        int[] input2 = {2, 1, 0, 3};

        char[] alphabet2 = {'v', 'o', 'l', 'e'};
        String expectedOutput2 = "love";
        assertEquals(expectedOutput2,
                     DigitsToStringConverter.convertDigitsToString(
                             input2, 4, alphabet2));
        
        int[] input3 = {0, 1, 2, 2, 3, 4};
        char[] alphabet3 = {'b', 'e', 'l', 'o', 'w'};
        String expectedOutput3 = "bellow";
        assertEquals(expectedOutput3,
                     DigitsToStringConverter.convertDigitsToString(
                             input3, 5, alphabet3));
    }
}
