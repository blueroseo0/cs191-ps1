package piwords;

import static org.junit.Assert.*;

import org.junit.Test;

public class PiGeneratorTest {
    @Test
    public void computePiInHexTest() {
        // 5^7 mod 23 = 17
    	int[] array={3,2,4,3,15,6}; //retrieved from http://www.mathsisfun.com/binary-decimal-hexadecimal-converter.html
        assertArrayEquals(array, PiGenerator.computePiInHex(6));
    }
    
    public void basicPowerModTest() {
        // 5^7 mod 23 = 17
        assertEquals(17, PiGenerator.powerMod(5, 7, 23));
        assertEquals(10, PiGenerator.powerMod(8, 2, 54));
        assertEquals(11, PiGenerator.powerMod(2, 8, 245));
        assertEquals(0, PiGenerator.powerMod(4, 3, 4));
        
    }
}
