package piwords;

import static org.junit.Assert.*;

import org.junit.Test;

public class AlphabetGeneratorTest {
    @Test
    public void generateFrequencyAlphabetTest() {
        // Expect in the training data that Pr(a) = 2/5, Pr(b) = 2/5,
        // Pr(c) = 1/5.
        String[] trainingData = {"aa", "bbc"};
        // In the output for base 10, they be in the same proportion.
        char[] expectedOutput = {'a', 'a', 'a', 'a',
                                 'b', 'b', 'b', 'b',
                                 'c', 'c'};
        assertArrayEquals(expectedOutput,
                AlphabetGenerator.generateFrequencyAlphabet(
                        10, trainingData));
    

	    String[] trainingData2 = {"aaaabcc", "bbc"};
	    char[] expectedOutput2 = {'a', 'a', 'a', 'a',
	                             'b', 'b', 'b', 'c',
	                             'c', 'c'};
	    assertArrayEquals(expectedOutput2,
	            AlphabetGenerator.generateFrequencyAlphabet(
	                    10, trainingData2));
	
		String[] trainingData3 = {"adee", "bbcddd"};
		char[] expectedOutput3 = {'a', 'b', 'b', 'c',
		                         'd', 'd', 'd', 'd',
		                         'e', 'e'};
		assertArrayEquals(expectedOutput3,
		        AlphabetGenerator.generateFrequencyAlphabet(
		                10, trainingData3));

    }
}
