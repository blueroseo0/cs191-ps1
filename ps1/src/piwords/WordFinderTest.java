package piwords;

import static org.junit.Assert.*;

import java.util.HashMap;
import java.util.Map;

import org.junit.Test;

public class WordFinderTest {
    @Test
    public void basicGetSubstringsTest() {
        String haystack = "abcde";
        String[] needles = {"ab", "abc", "de", "fg"};

        Map<String, Integer> expectedOutput = new HashMap<String, Integer>();
        expectedOutput.put("ab", 0);
        expectedOutput.put("abc", 0);
        expectedOutput.put("de", 3);

        assertEquals(expectedOutput, WordFinder.getSubstrings(haystack,
                                                              needles));
        
        String haystack2 = "jedarenathanarah";
        String[] needles2 = {"jed", "daren", "nathan", "arah"};

        Map<String, Integer> expectedOutput2 = new HashMap<String, Integer>();
        expectedOutput2.put("jed", 0);
        expectedOutput2.put("daren", 2);
        expectedOutput2.put("nathan", 6);
        expectedOutput2.put("arah", 12);
        
        assertEquals(expectedOutput2, WordFinder.getSubstrings(haystack2,
                                                              needles2));
        
        String haystack3 = "wthis";
        String[] needles3 = {"wth", "is", "this"};

        Map<String, Integer> expectedOutput3 = new HashMap<String, Integer>();
        expectedOutput3.put("wth", 0);
        expectedOutput3.put("is", 3);
        expectedOutput3.put("this", 1);

        assertEquals(expectedOutput3, WordFinder.getSubstrings(haystack3,
                                                              needles3));
        
        
    }
}
